import {Housing} from "./housing";
import {Condition} from "./condition";
import {Category} from "./category";

export class Rooms {
    id:number;

    constructor(public name:string,
                public categoryRoom:Category,
                public conditionRoom:Condition,
                public housing:Housing) {
    }
}