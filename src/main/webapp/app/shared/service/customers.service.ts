import { Http,Headers,RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {Customers} from "../model/customers";

@Injectable()
export class CustomersService {

    private apiUrl = '/artezio/customers';

    constructor(private http:Http) {
    }

    getCustomers() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get(this.apiUrl)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError);

    }

    createCustomers(customer:Customers) {
        if (!!!customer.id) {
            return this.addCustomers(customer);
        }
        if (customer.id) {
            return this.updateCustomers(customer);
        }
    }

    addCustomers(customer:Customers) {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post(this.apiUrl, customer, this.createOption)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError)
    }

    updateCustomers(customer:Customers) {
        let url = `${this.apiUrl}/${customer.id}`;
        //noinspection TypeScriptUnresolvedFunction
        return this.http.put(url, customer, this.createOption)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError)
    }

    deleteCustomers(customer:Customers) {
        let url = `${this.apiUrl}/${customer.id}`;

        //noinspection TypeScriptUnresolvedFunction
        return this.http.delete(url, this.createOption)
            .map(res => {
                    if (!res.json().success) {
                        alert(res.json().errorMessage);
                        return false
                    } else {
                        return true;
                    }
                }
            )
            .catch(this.handleError);
    }

    createOption() {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        return options;
    }

    handlingResponse(res:any) {
        if (!res.json().success) {
            alert(res.json().errorMessage);
        } else {
            return res.json().result as Customers[];
        }
    }

    private handleError(error:any) {
        console.error('Произошла ошибка');
        return Observable.throw(error.message || error);
    }
}