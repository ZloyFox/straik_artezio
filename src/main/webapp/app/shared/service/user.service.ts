import { Http,Headers,RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {User} from "../model/user";

@Injectable()
export class UserService {

    private apiUrl = '/artezio/users';

    constructor(private http:Http) {
    }

    getUsers() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get(this.apiUrl)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError);
    }

    createUser(user:User) {
        if (!!!user.id) {
            return this.addUser(user);
        }
        if (user.id) {
            return this.updateUser(user);
        }
    }

    addUser(user:User) {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post(this.apiUrl, user, this.createOption)
            .map(res => this.handlingResponse(res))
            .catch(this.handleError)
    }

    updateUser(user:User) {
        let url = `${this.apiUrl}/${user.id}`;
        //noinspection TypeScriptUnresolvedFunction
        return this.http.put(url, user, this.createOption)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError)
    }

    deleteUser(user:User) {
        let url = `${this.apiUrl}/${user.id}`;

        //noinspection TypeScriptUnresolvedFunction
        return this.http.delete(url, this.createOption)
            .map(res => {
                    if (!res.json().success) {
                        alert(res.json().errorMessage);
                        return false
                    } else {
                        return true;
                    }
                }
            )
            .catch(this.handleError);
    }

    createOption() {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        return options;
    }

    handlingResponse(res:any) {
        if (!res.json().success) {
            alert(res.json().errorMessage);
        } else {
            return res.json().result as User[];
        }
    }

    private handleError(error:any) {
        console.error('Произошла ошибка');
        return Observable.throw(error.message || error);
    }
}