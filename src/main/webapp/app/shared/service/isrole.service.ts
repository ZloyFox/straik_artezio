import { Http,Headers,RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {Housing} from "../model/housing";

@Injectable()
export class AuthService {

    private name:string;
    private roles:[{authority:string}];
    private apiUrl = '/artezio/logged/';

    constructor(private http:Http) {
    }

    getLoggedName() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get(this.apiUrl + 'name')
            .map(res =>
                res.json()
            )
            .catch(this.handleError);
    }

    initRole() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get(this.apiUrl + 'roles')
            .map(res => res.json() as [{authority:string}])
            .catch(this.handleError);
    }

    private handleError(error:any) {
        console.error('Произошла ошибка');
        return Observable.throw(error.message || error);
    }
}