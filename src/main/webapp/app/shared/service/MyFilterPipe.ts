import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'myfilter',
    pure: false
})
export class MyFilterPipe implements PipeTransform {
    transform(items: any[], field : string, value : any): any[] {
        if(!field)return items;
        if(!value)return items;
        if (!items) return [];
        return items.filter(it => (it[field]).indexOf(value)==0);
    }
}