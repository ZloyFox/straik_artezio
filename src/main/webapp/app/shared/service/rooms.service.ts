import { Http,Headers,RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {Rooms} from "../model/rooms";

@Injectable()
export class RoomsService {

    private apiUrl = '/artezio/rooms';

    constructor(private http:Http) {
    }

    getRooms() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get(this.apiUrl)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError);
    }

    createRooms(rooms:Rooms) {
        if (!!!rooms.id) {
            return this.addRooms(rooms);
        }
        if (rooms.id) {
            return this.updateRooms(rooms);
        }
    }

    addRooms(rooms:Rooms) {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post(this.apiUrl, rooms, this.createOption)
            .map(res => this.handlingResponse(res))
            .catch(this.handleError)
    }

    updateRooms(rooms:Rooms) {
        let url = `${this.apiUrl}/${rooms.id}`;
        //noinspection TypeScriptUnresolvedFunction
        return this.http.put(url, rooms, this.createOption)
            .map(res =>this.handlingResponse(res))
            .catch(this.handleError)
    }

    deleteRooms(rooms:Rooms) {
        let url = `${this.apiUrl}/${rooms.id}`;
        //noinspection TypeScriptUnresolvedFunction
        return this.http.delete(url, this.createOption)
            .map(res => {
                    if (!res.json().success) {
                        alert(res.json().errorMessage);
                        return false
                    } else {
                        return true;
                    }
                }
            )
            .catch(this.handleError);
    }

    createOption() {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        return options;
    }

    handlingResponse(res:any) {
        if (!res.json().success) {
            alert(res.json().errorMessage);
        } else {
            return res.json().result as Rooms[];
        }
    }

    private handleError(error:any) {
        console.error('Произошла ошибка');
        return Observable.throw(error.message || error);
    }
}