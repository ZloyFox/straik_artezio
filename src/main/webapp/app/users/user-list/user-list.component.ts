import {Component, Input, Output, EventEmitter} from '@angular/core';
import {User} from "../../shared/model/user";

@Component({
    moduleId: module.id,
    selector: 'user-list',
    templateUrl: 'user-list.component.html',
    styleUrls: ['user-list.component.css']
})

export class UserListComponent {

    fieldUser:{name:string,value:string}[] = [
        {name: "логин", value: "login"},
        {name: "статус", value: "status"},
        {name: "эл. почта", value: "email"}
    ];
    filterValue:string = '';

    @Input() users:User[];
    @Output() delete:EventEmitter<User> = new EventEmitter();
    @Output() update:EventEmitter<User> = new EventEmitter();

    onUpdate(user:any) {
        this.update.emit(user);
    }

    onDelete(user:User) {
        this.delete.emit(user);
    }
}