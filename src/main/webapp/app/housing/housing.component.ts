import {Component, OnInit} from '@angular/core';

import {Housing} from "../shared/model/housing";
import {HousingService} from "../shared/service/housing.service";


@Component({
    moduleId: module.id,
    selector: 'housing',
    templateUrl: 'housing.component.html',
    styleUrls: ['housing.component.css']
})

export class HousingComponent implements OnInit {
    housings:Housing[];
    housing:{housingIn:Housing, flaq:number};

    constructor(private housingService:HousingService) {
        this.housings = [];
    }

    ngOnInit() {
        this.housingService.getHousing().subscribe(housings => this.housings = housings);
    }

    create(housing:Housing) {
        this.housingService.createHousing(housing).subscribe(
            housingResponse=> {
                if (!!housingResponse) {
                    if (!!housing.id) {
                        let index = this.findIndex(housingResponse);
                        this.housings[index] = housingResponse;
                    } else {
                        this.housings.push(housingResponse);
                    }

                }
            });
    }

    findIndex(item:Housing):number {
        for (var i = 0; i < this.housings.length; i++) {
            if (this.housings[i].id == item.id) {
                return i;
            }
        }
    }

    update(housing:Housing) {
        this.housing = {
            housingIn: housing,
            flaq: Math.random()
        }
    }

    delete(housing:Housing) {
        this.housingService.deleteHousing(housing).subscribe(
            res => {
                if (res) {
                    let index = this.housings.indexOf(housing);
                    if (index > -1) {
                        this.housings.splice(index, 1);
                    }
                }
            });
    }
}