import {Component, Input, Output, EventEmitter } from '@angular/core';
import {Housing} from "../../shared/model/housing";

@Component({
    moduleId: module.id,
    selector: 'housing-item',
    templateUrl: 'housing-item.component.html',
    styleUrls: ['housing-item.component.css']
})

export class HousingItemComponent {
    @Input() housing:Housing;
    @Output() delete = new EventEmitter();
    @Output() update = new EventEmitter();

    onUpdate() {
        this.update.emit(this.housing);
    }

    onDelete() {
        if(confirm("Вы хотите удалить запись?")){
            this.delete.emit(this.housing);
        }
    }
}