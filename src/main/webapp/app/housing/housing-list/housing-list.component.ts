import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Housing} from "../../shared/model/housing";

@Component({
    moduleId: module.id,
    selector: 'housing-list',
    templateUrl: 'housing-list.component.html',
    styleUrls: ['housing-list.component.css']
})

export class HousingListComponent {
    fieldHousing:{name:string,value:string}[] = [
        {name: "корпус", value: "name"}
    ];

    @Input() housings:Housing[];
    @Output() delete:EventEmitter<Housing> = new EventEmitter();
    @Output() update:EventEmitter<Housing> = new EventEmitter();

    onUpdate(housing:Housing) {
        this.update.emit(housing);
    }

    onDelete(housing:Housing) {
        this.delete.emit(housing);
    }
}