import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.components';
import { UsersComponent } from './users/users.component';
import { UserFormComponent } from './users/user-form/user-form.component';
import { UserListComponent } from './users/user-list/user-list.component';
import { UserItemComponent } from './users/user-item/user-item.component';
import { UserService } from "./shared/service/user.service";
import {RoomsService} from "./shared/service/rooms.service";
import {HousingService} from "./shared/service/housing.service";
import {CustomersService} from "./shared/service/customers.service";
import {ConditionService} from "./shared/service/condition.service";
import {CategoryService} from "./shared/service/category.service";
import {CategoryComponent} from "./category/category.component";
import {CategoryFormComponent} from "./category/category-form/category-form.component";
import {CategoryListComponent} from "./category/category-list/category-list.component";
import {CategoryItemComponent} from "./category/category-item/category-item.component";
import {ConditionComponent} from "./condition/condition.component";
import {ConditionFormComponent} from "./condition/condition-form/condition-form.component";
import {ConditionListComponent} from "./condition/condition-list/condition-list.component";
import {ConditionItemComponent} from "./condition/condition-item/condition-item.component";
import {CustomersComponent} from "./customers/customers.component";
import {CustomersFormComponent} from "./customers/customers-form/customers-form.component";
import {CustomersListComponent} from "./customers/customers-list/customers-list.component";
import {CustomersItemComponent} from "./customers/customers-item/customers-item.component";
import {HousingComponent} from "./housing/housing.component";
import {HousingFormComponent} from "./housing/housing-form/housing-form.component";
import {HousingListComponent} from "./housing/housing-list/housing-list.component";
import {HousingItemComponent} from "./housing/housing-item/housing-item.component";
import {RoomsComponent} from "./rooms/rooms.component";
import {RoomsFormComponent} from "./rooms/rooms-form/rooms-form.component";
import {RoomsListComponent} from "./rooms/rooms-list/rooms-list.component";
import {RoomsItemComponent} from "./rooms/rooms-item/rooms-item.component";
import {MyFilterPipe} from "./shared/service/MyFilterPipe";
import {AuthService} from "./shared/service/isrole.service";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    declarations: [
        AppComponent,
        UsersComponent,
        UserFormComponent,
        UserListComponent,
        UserItemComponent,
        CategoryComponent,
        CategoryFormComponent,
        CategoryListComponent,
        CategoryItemComponent,
        ConditionComponent,
        ConditionFormComponent,
        ConditionListComponent,
        ConditionItemComponent,
        CustomersComponent,
        CustomersFormComponent,
        CustomersListComponent,
        CustomersItemComponent,
        HousingComponent,
        HousingFormComponent,
        HousingListComponent,
        HousingItemComponent,
        RoomsComponent,
        RoomsFormComponent,
        RoomsListComponent,
        RoomsItemComponent,
        MyFilterPipe
    ],
    providers: [UserService,
        RoomsService,
        HousingService,
        CustomersService,
        ConditionService,
        CategoryService,
        AuthService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}