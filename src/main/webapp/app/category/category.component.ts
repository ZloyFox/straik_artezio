import {Component, OnInit} from '@angular/core';
import {Category} from "../shared/model/category";
import {CategoryService} from "../shared/service/category.service";

@Component({
    moduleId: module.id,
    selector: 'category',
    templateUrl: 'category.component.html',
    styleUrls: ['category.component.css']
})

export class CategoryComponent implements OnInit {
    categorys:Category[];
    category:{categoryIn:Category, flaq:number};

    constructor(private categoryService:CategoryService) {
        this.categorys = [];
    }

    ngOnInit() {
        this.categoryService.getCategory().subscribe(categorys => this.categorys = categorys);
    }

    create(category:Category) {
        this.categoryService.createCategory(category).subscribe(
            categoryResponse=> {
                if (!!categoryResponse) {
                    if (!!category.id) {
                        let index = this.findTodoIndex(categoryResponse);
                        this.categorys[index] = categoryResponse;
                    } else {
                        this.categorys.push(categoryResponse);
                    }

                }
            });
    }

    findTodoIndex(item:Category):number {
        for (var i = 0; i < this.categorys.length; i++) {
            if (this.categorys[i].id == item.id) {
                return i;
            }
        }
    }

    update(category:Category) {
        this.category = {
            categoryIn: category,
            flaq: Math.random()
        }
    }

    delete(category:Category) {
        this.categoryService.deleteCategory(category).subscribe(
            res => {
                if (res) {
                    let index = this.categorys.indexOf(category);
                    if (index > -1) {
                        this.categorys.splice(index, 1);
                    }
                }
            });
    }
}