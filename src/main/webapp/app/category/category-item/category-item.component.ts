import {Component, Input, Output, EventEmitter } from '@angular/core';
import {Category} from "../../shared/model/category";

@Component({
    moduleId: module.id,
    selector: 'category-item',
    templateUrl: 'category-item.component.html',
    styleUrls: ['category-item.component.css']
})

export class CategoryItemComponent {
    @Input() category:Category;
    @Output() delete = new EventEmitter();
    @Output() update = new EventEmitter();

    onUpdate() {
        this.update.emit(this.category);
    }

    onDelete() {
        if(confirm("Вы хотите удалить запись?")){
            this.delete.emit(this.category);
        }
    }
}