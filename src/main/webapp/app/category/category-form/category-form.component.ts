import { Component, Output, Input, EventEmitter, OnChanges } from '@angular/core';
import {Category} from "../../shared/model/category";

@Component({
    moduleId: module.id,
    selector: 'category-form',
    templateUrl: 'category-form.component.html',
    styleUrls: ['category-form.component.css']
})
export class CategoryFormComponent implements OnChanges {

    name:string = '';
    pricePerDay:string = '';

    @Input() category:any;
    @Output() create:EventEmitter<Category> = new EventEmitter();

    ngOnChanges() {
        if ((!!this.category)) {
            this.name = this.category.categoryIn.name;
            this.pricePerDay = this.category.categoryIn.pricePerDay;
        }
    }

    onSubmit() {
        if (this.category) {
            let categoryUp:Category = new Category(this.name, this.pricePerDay);
            categoryUp.id = this.category.categoryIn.id;
            this.create.emit(categoryUp);
        }
        if (!this.category) {
            let category:Category = new Category(this.name, this.pricePerDay);
            this.create.emit(category);
        }
        this.onReset();
    }

    onReset() {
        this.category = undefined;
        this.name = '';
        this.pricePerDay = '';
    }
}