import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Category} from "../../shared/model/category";

@Component({
    moduleId: module.id,
    selector: 'category-list',
    templateUrl: 'category-list.component.html',
    styleUrls: ['category-list.component.css']
})

export class CategoryListComponent {
    fieldCategory:{name:string,value:string}[] = [
        {name: "категория", value: "name"},
        {name: "стоимость", value: "pricePerDay"}
    ];
    @Input() categorys:Category[];
    @Output() delete:EventEmitter<Category> = new EventEmitter();
    @Output() update:EventEmitter<Category> = new EventEmitter();

    onUpdate(category:Category) {
        this.update.emit(category);
    }

    onDelete(category:Category) {
        this.delete.emit(category);
    }
}