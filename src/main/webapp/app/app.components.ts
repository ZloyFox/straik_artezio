import { Component, OnInit } from '@angular/core';
import {Http} from "@angular/http";
import { Observable } from 'rxjs/Observable';
import {AuthService} from "./shared/service/isrole.service";


@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.css']
})
export class AppComponent implements OnInit {
    titleHead:string = 'Sanatorium';
    title:string = '';
    test:string = 'IsActive';
    userMenu:boolean = false;
    categoryMenu:boolean = false;
    conditionMenu:boolean = false;
    customersMenu:boolean = false;
    housingMenu:boolean = false;
    roomsMenu:boolean = false;
    isAdmin:boolean = false;
    isGuest:boolean = false;
    isUser:boolean = false;

    constructor(private http:Http, private authService:AuthService) {
    }

    ngOnInit() {
        this.authService.getLoggedName().subscribe(res => {
            this.test = res as string
        });
        this.authService.initRole().subscribe(res=> {
            for (let item of res) {
                if (item.authority == 'ROLE_ADMIN')this.isAdmin = true;
                if (item.authority == 'ROLE_GUEST')this.isGuest = true;
                if (item.authority == 'ROLE_USER')this.isUser = true;
            }
        });
    }

    onShowUserMenu() {
        this.hideAll();
        this.createTittle('Пользователи');
        this.userMenu = !this.userMenu;
    }

    onShowCategoryMenu() {
        this.hideAll();
        this.createTittle('Категория номера');
        this.categoryMenu = !this.categoryMenu;
    }

    onShowConditionMenu() {
        this.hideAll();
        this.createTittle('Состояние номера');
        this.conditionMenu = !this.conditionMenu;
    }

    onShowCustomersMenu() {
        this.hideAll();
        this.createTittle('Клиенты');
        this.customersMenu = !this.customersMenu;
    }

    onShowHousingMenu() {
        this.hideAll();
        this.createTittle('Корпуса');
        this.housingMenu = !this.housingMenu;
    }

    onShowRoomsMenu() {
        this.hideAll();
        this.createTittle('Номера');
        this.roomsMenu = !this.roomsMenu;
    }

    hideAll() {
        //this.title = this.titleHead;
        this.userMenu = false;
        this.categoryMenu = false;
        this.conditionMenu = false;
        this.customersMenu = false;
        this.housingMenu = false;
        this.roomsMenu = false;
    }

    createTittle(str:string) {
        this.title = str;
    }

    private handleError(error:any) {
        console.error('Произошла ошибка');
        return Observable.throw(error.message || error);
    }
}