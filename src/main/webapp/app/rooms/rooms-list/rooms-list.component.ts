import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Rooms} from "../../shared/model/rooms";

@Component({
    moduleId: module.id,
    selector: 'rooms-list',
    templateUrl: 'rooms-list.component.html',
    styleUrls: ['rooms-list.component.css']
})

export class RoomsListComponent {
    fieldRoom:{name:string,value:string}[] = [
        {name: "номер", value: "name"},
        //{name: "категория", value: "categoryRoom.name"},
        //{name: "состояние", value: "conditionRoom.name"},
        //{name: "корпус", value: "housing.name"}
    ];
    @Input() rooms:Rooms[];
    @Output() delete:EventEmitter<Rooms> = new EventEmitter();
    @Output() update:EventEmitter<Rooms> = new EventEmitter();

    onUpdate(room:Rooms) {
        this.update.emit(room);
    }

    onDelete(room:Rooms) {
        this.delete.emit(room);
    }
}