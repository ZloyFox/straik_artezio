import {Component, OnInit} from '@angular/core';

import {Rooms} from "../shared/model/rooms";
import {RoomsService} from "../shared/service/rooms.service";


@Component({
    moduleId: module.id,
    selector: 'rooms',
    templateUrl: 'rooms.component.html',
    styleUrls: ['rooms.component.css']
})

export class RoomsComponent implements OnInit {
    rooms:Rooms[];
    room:{roomIn:Rooms, flaq:number};

    constructor(private roomsService:RoomsService) {
        this.rooms = [];
    }

    ngOnInit() {
        this.roomsService.getRooms().subscribe(rooms => this.rooms = rooms);
    }

    create(room:Rooms) {
        this.roomsService.createRooms(room).subscribe(
            roomResponse=> {
                if (!!roomResponse) {
                    if (!!room.id) {
                        let index = this.findIndex(roomResponse);
                        this.rooms[index] = roomResponse;
                    } else {
                        this.rooms.push(roomResponse);
                    }

                }
            });
    }

    findIndex(item:Rooms):number {
        for (var i = 0; i < this.rooms.length; i++) {
            if (this.rooms[i].id == item.id) {
                return i;
            }
        }
    }

    update(room:Rooms) {
        this.room = {
            roomIn: room,
            flaq: Math.random()
        }
    }

    delete(room:Rooms) {
        this.roomsService.deleteRooms(room).subscribe(
            res => {
                if (res) {
                    let index = this.rooms.indexOf(room);
                    if (index > -1) {
                        this.rooms.splice(index, 1);
                    }
                }
            });
    }
}