import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Condition} from "../../shared/model/condition";

@Component({
    moduleId: module.id,
    selector: 'condition-list',
    templateUrl: 'condition-list.component.html',
    styleUrls: ['condition-list.component.css']
})

export class ConditionListComponent {
    fieldCondition:{name:string,value:string}[] = [
        {name: "состояние", value: "name"}
    ];
    @Input() conditions:Condition[];
    @Output() delete:EventEmitter<Condition> = new EventEmitter();
    @Output() update:EventEmitter<Condition> = new EventEmitter();

    onUpdate(condition:Condition) {
        this.update.emit(condition);
    }

    onDelete(condition:Condition) {
        this.delete.emit(condition);
    }
}