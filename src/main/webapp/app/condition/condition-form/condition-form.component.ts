import { Component, Output, Input, EventEmitter, OnChanges } from '@angular/core';
import {Condition} from "../../shared/model/condition";

@Component({
    moduleId: module.id,
    selector: 'condition-form',
    templateUrl: 'condition-form.component.html',
    styleUrls: ['condition-form.component.css']
})
export class ConditionFormComponent implements OnChanges{
    name:string = '';

    @Input() condition:any;
    @Output() create:EventEmitter<Condition> = new EventEmitter();

    ngOnChanges() {
        if ((!!this.condition)) {
            this.name = this.condition.conditionIn.name;
        }
    }

    onSubmit() {
        if (this.condition) {
            let conditionUp:Condition = new Condition(this.name);
            conditionUp.id = this.condition.conditionIn.id;
            this.create.emit(conditionUp);
        }
        if (!this.condition) {
            let condition:Condition = new Condition(this.name);
            this.create.emit(condition);
        }
        this.onReset();
    }

    onReset() {
        this.condition = undefined;
        this.name = '';
    }
}