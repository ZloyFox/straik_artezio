import {Component, Input, Output, EventEmitter } from '@angular/core';
import {Condition} from "../../shared/model/condition";

@Component({
    moduleId: module.id,
    selector: 'condition-item',
    templateUrl: 'condition-item.component.html',
    styleUrls: ['condition-item.component.css']
})

export class ConditionItemComponent {
    @Input() condition:Condition;
    @Output() delete = new EventEmitter();
    @Output() update = new EventEmitter();

    onUpdate() {
        this.update.emit(this.condition);
    }

    onDelete() {
        if(confirm("Вы хотите удалить запись?")){
            this.delete.emit(this.condition);
        }
    }
}