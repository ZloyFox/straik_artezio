package ru.artezio.exceptions;

import java.util.Map;

public class ValidateException extends ApiException {
    private Map<String, String> errorFieldWithMsg;

    public Map<String, String> getErrorFieldWithMsg() {
        return errorFieldWithMsg;
    }

    public ValidateException(Map<String, String> errorFieldWithMsg) {
        this.errorFieldWithMsg = errorFieldWithMsg;
        setMessage(errorFieldWithMsg.toString());
    }

    public ValidateException(String error) {
        setMessage(error);
    }
}
