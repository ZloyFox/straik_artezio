package ru.artezio.exceptions;

public class InternalException extends ApiException {
    @Override
    public String getMessage() {
        return "Упс... Что-то пошло не так!";
    }
}
