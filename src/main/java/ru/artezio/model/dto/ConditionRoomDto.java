package ru.artezio.model.dto;

import org.hibernate.validator.constraints.NotBlank;

public class ConditionRoomDto {

    private Long id;
    @NotBlank(message = "Состояние номера не указано.")
    private String name;

    public ConditionRoomDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ConditionRoomDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
