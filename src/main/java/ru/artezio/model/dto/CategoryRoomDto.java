package ru.artezio.model.dto;

import org.hibernate.validator.constraints.NotBlank;

public class CategoryRoomDto {

    private Long id;
    @NotBlank(message = "Категория номера не указана.")
    private String name;
    @NotBlank(message = "Цена в сутки не указана.")
    private String pricePerDay;

    public CategoryRoomDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(String pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    @Override
    public String toString() {
        return "CategoryRoomDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pricePerDay='" + pricePerDay + '\'' +
                '}';
    }
}
