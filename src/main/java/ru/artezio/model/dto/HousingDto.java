package ru.artezio.model.dto;

import org.hibernate.validator.constraints.NotBlank;

public class HousingDto {

    private Long id;
    @NotBlank(message = "Корпус не указан.")
    private String name;

    public HousingDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "HousingDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
