package ru.artezio.model.dto;

import org.hibernate.validator.constraints.NotBlank;
import ru.artezio.model.CategoryRoom;
import ru.artezio.model.ConditionRoom;
import ru.artezio.model.Housing;

public class RoomsDto {

    private Long id;
    @NotBlank(message = "Номер не указан.")
    private String name;
    private CategoryRoom categoryRoom;
    private ConditionRoom conditionRoom;
    private Housing housing;

    public RoomsDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryRoom getCategoryRoom() {
        return categoryRoom;
    }

    public void setCategoryRoom(CategoryRoom categoryRoom) {
        this.categoryRoom = categoryRoom;
    }

    public ConditionRoom getConditionRoom() {
        return conditionRoom;
    }

    public void setConditionRoom(ConditionRoom conditionRoom) {
        this.conditionRoom = conditionRoom;
    }

    public Housing getHousing() {
        return housing;
    }

    public void setHousing(Housing housing) {
        this.housing = housing;
    }

    @Override
    public String toString() {
        return "RoomsDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryRoom=" + categoryRoom +
                ", conditionRoom=" + conditionRoom +
                ", housing=" + housing +
                '}';
    }
}
