package ru.artezio.model;

import javax.persistence.*;
import javax.persistence.Entity;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.Table;

@Entity
@Table(name = "rooms")
@NamedQueries({
        @NamedQuery(name = "findAllRooms", query = "SELECT c FROM Rooms c")
})
public class Rooms extends BaseEntity<Long> {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false) private String name;

    @ManyToOne@JoinColumn(name = "category_room") private CategoryRoom categoryRoom;
    @ManyToOne@JoinColumn(name = "condition_room") private ConditionRoom conditionRoom;
    @ManyToOne@JoinColumn(name = "housing") private Housing housing;

    public Rooms() {
    }

    public Rooms(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CategoryRoom getCategoryRoom() {
        return categoryRoom;
    }

    public void setCategoryRoom(CategoryRoom categoryRoom) {
        this.categoryRoom = categoryRoom;
    }

    public ConditionRoom getConditionRoom() {
        return conditionRoom;
    }

    public void setConditionRoom(ConditionRoom conditionRoom) {
        this.conditionRoom = conditionRoom;
    }

    public Housing getHousing() {
        return housing;
    }

    public void setHousing(Housing housing) {
        this.housing = housing;
    }

    @Override
    public String toString() {
        return "Rooms{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryRoom=" + categoryRoom +
                ", conditionRoom=" + conditionRoom +
                ", housing=" + housing +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rooms rooms = (Rooms) o;

        if (id != null ? !id.equals(rooms.id) : rooms.id != null) return false;
        if (name != null ? !name.equals(rooms.name) : rooms.name != null) return false;
        if (categoryRoom != null ? !categoryRoom.equals(rooms.categoryRoom) : rooms.categoryRoom != null) return false;
        if (conditionRoom != null ? !conditionRoom.equals(rooms.conditionRoom) : rooms.conditionRoom != null)
            return false;
        return !(housing != null ? !housing.equals(rooms.housing) : rooms.housing != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (categoryRoom != null ? categoryRoom.hashCode() : 0);
        result = 31 * result + (conditionRoom != null ? conditionRoom.hashCode() : 0);
        result = 31 * result + (housing != null ? housing.hashCode() : 0);
        return result;
    }
}
