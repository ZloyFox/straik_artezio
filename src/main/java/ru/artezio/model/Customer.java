package ru.artezio.model;

import javax.persistence.*;
import javax.persistence.Entity;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.Table;

@Entity
@Table(name = "customers")
@NamedQueries({
        @NamedQuery(name = "findAllCustomers", query = "SELECT c FROM Customer c")
})
public class Customer extends BaseEntity<Long> {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false) private String name;
    @Column(name = "surname", nullable = false) private String surname;
    @Column(name = "patronymic", nullable = false) private String patronymic;
    @Column(name = "passportSeries") private String passportSeries;
    @Column(name = "passportId") private Integer passportId;
    @Column(name = "email", nullable = false) private String email;
    @Column(name = "phone", nullable = false) private String phone;

    public Customer() {
    }

    public Customer(String name, String surname, String patronymic, String email, String phone) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.email = email;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPassportSeries() {
        return passportSeries;
    }

    public void setPassportSeries(String passportSeries) {
        this.passportSeries = passportSeries;
    }

    public Integer getPassportId() {
        return passportId;
    }

    public void setPassportId(Integer passportId) {
        this.passportId = passportId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", passportSeries='" + passportSeries + '\'' +
                ", passportId=" + passportId +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customers = (Customer) o;

        if (id != null ? !id.equals(customers.id) : customers.id != null) return false;
        if (name != null ? !name.equals(customers.name) : customers.name != null) return false;
        if (surname != null ? !surname.equals(customers.surname) : customers.surname != null) return false;
        if (patronymic != null ? !patronymic.equals(customers.patronymic) : customers.patronymic != null) return false;
        if (passportSeries != null ? !passportSeries.equals(customers.passportSeries) : customers.passportSeries != null)
            return false;
        if (passportId != null ? !passportId.equals(customers.passportId) : customers.passportId != null)
            return false;
        if (email != null ? !email.equals(customers.email) : customers.email != null) return false;
        return !(phone != null ? !phone.equals(customers.phone) : customers.phone != null);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        result = 31 * result + (passportSeries != null ? passportSeries.hashCode() : 0);
        result = 31 * result + (passportId != null ? passportId.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }
}
