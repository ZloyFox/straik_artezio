package ru.artezio.service;

import ru.artezio.model.dto.CustomerDto;

import java.util.List;

public interface CustomerService {

    List<CustomerDto> list();

    public CustomerDto create(CustomerDto customer);

    public CustomerDto findById(Long id);

    public void remove(Long id);
}
