package ru.artezio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.artezio.dao.UserDao;
import ru.artezio.dao.UserRoleDao;
import ru.artezio.model.User;
import ru.artezio.model.UserRole;
import ru.artezio.model.dto.UserDto;
import ru.artezio.service.UserService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl extends BaseService implements UserService {

    @Autowired UserDao userDao;
    @Autowired UserRoleDao userRoleDao;

    public List<UserDto> list() {
        return mappingList(userDao.list(), UserDto.class);
    }

    public UserDto create(UserDto user) {
        user = encodeUserPassword(user);
        user.getRoles().add(userRoleDao.findById(3L));
        return mappingObject(userDao.create(mappingObject(user, User.class)), UserDto.class);
    }

    public UserDto findById(Long id) {
        return mappingObject(userDao.findById(id), UserDto.class);
    }

    @Override
    public void remove(Long id) {
        userDao.remove(id);
    }

    @Override
    public UserDto findByLogin(String login) {
        User user = userDao.findByLogin(login);
        if(user == null)return null;
        return mappingObject(user, UserDto.class);
    }

    @Override
    public List<UserRole> listUserRole() {
        return userRoleDao.list();
    }

    public UserDto encodeUserPassword(UserDto user) {
        String hashString = user.getPassword();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String hashOutput = bCryptPasswordEncoder.encode(hashString);
        user.setPassword(hashOutput);
        return user;
    }
}