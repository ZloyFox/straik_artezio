package ru.artezio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.artezio.dao.ConditionRoomDao;
import ru.artezio.model.ConditionRoom;
import ru.artezio.model.dto.ConditionRoomDto;
import ru.artezio.service.ConditionRoomService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ConditionRoomServiceImpl extends BaseService implements ConditionRoomService {

    @Autowired ConditionRoomDao conditionRoomDao;

    @Override
    public List<ConditionRoomDto> list() {
        return mappingList(conditionRoomDao.list(), ConditionRoomDto.class);
    }

    @Override
    public ConditionRoomDto create(ConditionRoomDto conditionRoom) {
        return mappingObject(conditionRoomDao.create(mappingObject(conditionRoom, ConditionRoom.class)), ConditionRoomDto.class);
    }

    @Override
    public ConditionRoomDto findById(Long id) {
        return mappingObject(conditionRoomDao.findById(id), ConditionRoomDto.class);
    }

    @Override
    public void remove(Long id) {
        conditionRoomDao.remove(id);
    }
}
