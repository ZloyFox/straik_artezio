package ru.artezio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.artezio.dao.CustomerDao;
import ru.artezio.model.Customer;
import ru.artezio.model.dto.CustomerDto;
import ru.artezio.service.CustomerService;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl extends BaseService implements CustomerService {

    @Autowired CustomerDao customerDao;

    @Override
    public List<CustomerDto> list() {
        return mappingList(customerDao.list(), CustomerDto.class);
    }

    @Override
    public CustomerDto create(CustomerDto customer) {
        return mappingObject(customerDao.create(mappingObject(customer, Customer.class)), CustomerDto.class);
    }

    @Override
    public CustomerDto findById(Long id) {
        return mappingObject(customerDao.findById(id), CustomerDto.class);
    }

    @Override
    public void remove(Long id) {
        customerDao.remove(id);
    }
}
