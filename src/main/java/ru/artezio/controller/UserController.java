package ru.artezio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.artezio.accessory.response.BaseResponse;
import ru.artezio.exceptions.ValidateException;
import ru.artezio.model.dto.UserDto;
import ru.artezio.service.UserService;

@RestController
@RequestMapping(value = "/users")
public class UserController extends BaseController {

    @Autowired private UserService userService;

    @RequestMapping
    public ResponseEntity<BaseResponse> listUsers() {
        try {
            return createSuccessResponse(userService.list());
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<BaseResponse> createUser(@RequestBody UserDto user) {
        try {
            validationUser(user);
            return createSuccessResponse(userService.create(user));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<BaseResponse> updateUser(@PathVariable Long id, @RequestBody UserDto user) {
        try {
            validationUser(user);
            user.setId(id);
            return createSuccessResponse(userService.create(user));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<BaseResponse> findUserById(@PathVariable Long id) {
        try {
            return createSuccessResponse(userService.findById(id));
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BaseResponse> deleteUser(@PathVariable Long id) {
        try {
            userService.remove(id);
            return createVoidResponse();
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    public void validationUser(UserDto user) throws ValidateException {
        validationObject(user);
        if (user.getId()!=null) {
            if (!user.getId().equals(userService.findById(user.getId()).getId()))
                if (userService.findByLogin(user.getLogin()) != null) {
                    throw new ValidateException("Такой логин уже существует");
                }
        }
    }
}