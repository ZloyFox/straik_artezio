package ru.artezio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.accessory.response.BaseResponse;
import ru.artezio.exceptions.ValidateException;
import ru.artezio.model.dto.CategoryRoomDto;
import ru.artezio.service.CategoryRoomService;

@RestController
@RequestMapping(value = "/categorys")
public class CategoryRoomController extends BaseController {

    @Autowired CategoryRoomService categoryRoomService;

    @RequestMapping
    public ResponseEntity<BaseResponse> listCategoryRooms() {
        try {
            return createSuccessResponse(categoryRoomService.list());
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<BaseResponse> createCategoryRoom(@RequestBody CategoryRoomDto categoryRoom) {
        try {
            validationObject(categoryRoom);
            return createSuccessResponse(categoryRoomService.create(categoryRoom));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<BaseResponse> updateCategoryRoom(@PathVariable Long id, @RequestBody CategoryRoomDto categoryRoom) {
        try {
            validationObject(categoryRoom);
            categoryRoom.setId(id);
            return createSuccessResponse(categoryRoomService.create(categoryRoom));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<BaseResponse> findCategoryRoomById(@PathVariable Long id) {
        try {
            return createSuccessResponse(categoryRoomService.findById(id));
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BaseResponse> deleteCategoryRoom(@PathVariable Long id) {
        try {
            categoryRoomService.remove(id);
            return createVoidResponse();
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }
}
