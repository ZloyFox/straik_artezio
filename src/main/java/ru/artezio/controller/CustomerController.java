package ru.artezio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.accessory.response.BaseResponse;
import ru.artezio.exceptions.ValidateException;
import ru.artezio.model.dto.CustomerDto;
import ru.artezio.service.CustomerService;

@RestController
@RequestMapping(value = "/customers")
public class CustomerController extends BaseController{

    @Autowired CustomerService customerService;

    @RequestMapping
    public ResponseEntity<BaseResponse> listCustomers() {
        try {
            return createSuccessResponse(customerService.list());
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<BaseResponse> createCustomer(@RequestBody CustomerDto customer) {
        try {
            validationObject(customer);
            return createSuccessResponse(customerService.create(customer));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<BaseResponse> updateCustomer(@PathVariable Long id, @RequestBody CustomerDto customer) {
        try {
            validationObject(customer);
            customer.setId(id);
            return createSuccessResponse(customerService.create(customer));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<BaseResponse> findCustomerById(@PathVariable Long id) {
        try {
            return createSuccessResponse(customerService.findById(id));
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BaseResponse> deleteCustomer(@PathVariable Long id) {
        try {
            customerService.remove(id);
            return createVoidResponse();
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }
}
