package ru.artezio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.artezio.accessory.response.BaseResponse;
import ru.artezio.exceptions.ValidateException;
import ru.artezio.model.dto.RoomsDto;
import ru.artezio.service.RoomsService;

@RestController
@RequestMapping(value = "/rooms")
public class RoomsController extends BaseController{

    @Autowired RoomsService roomsService;

    @RequestMapping
    public ResponseEntity<BaseResponse> listRooms() {
        try {
            return createSuccessResponse(roomsService.list());
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<BaseResponse> createRoom(@RequestBody RoomsDto rooms) {
        try {
            validationObject(rooms);
            return createSuccessResponse(roomsService.create(rooms));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<BaseResponse> updateRoom(@PathVariable Long id, @RequestBody RoomsDto rooms) {
        try {
            validationObject(rooms);
            rooms.setId(id);
            return createSuccessResponse(roomsService.create(rooms));
        } catch (ValidateException exception) {
            return createFailedResponse(exception);
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<BaseResponse> findRoomById(@PathVariable Long id) {
        try {
            return createSuccessResponse(roomsService.findById(id));
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<BaseResponse> deleteRoom(@PathVariable Long id) {
        try {
            roomsService.remove(id);
            return createVoidResponse();
        } catch (Exception exception) {
            return createInternalExceptionResponse();
        }
    }
}
