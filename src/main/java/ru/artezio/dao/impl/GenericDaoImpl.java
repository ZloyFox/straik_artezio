package ru.artezio.dao.impl;

import org.springframework.stereotype.Repository;
import ru.artezio.accessory.QueryParam;
import ru.artezio.model.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@Repository
@Transactional
public abstract class GenericDaoImpl<T extends BaseEntity<U>, U> {

    @PersistenceContext private EntityManager entityManager;
    private Class<T> entityClass;

    @SuppressWarnings("unchecked")
    public GenericDaoImpl() {
        this.entityClass = (Class<T>) (((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments())[0];
    }

    @SuppressWarnings("unchecked")
    protected List<T> findObjectsByNamedQuery(String nameQuery, List<QueryParam> params) {
        Query query = entityManager.createNamedQuery(nameQuery);
        for (QueryParam param : params)
            query.setParameter(param.getName(), param.getValue());
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    protected List<T> findObjectsByNamedQuery(String nameQuery) {
        return entityManager.createNamedQuery(nameQuery).getResultList();
    }

    protected T save(T entity) {
        T entityMerge = entityManager.merge(entity);
        entityManager.persist(entityMerge);
        return entityMerge;
    }

    @SuppressWarnings("unchecked")
    protected T findObjectByNamedQuery(String nameQuery, List<QueryParam> params) {
        Query query = entityManager.createNamedQuery(nameQuery);
        for (QueryParam param : params)
            query.setParameter(param.getName(), param.getValue());
        try {
            return (T) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    protected void persist(T entity) {
        entityManager.persist(entity);
    }

    protected T update(T entity) {
        return entityManager.merge(entity);
    }

    protected T findById(U id) {
        return entityManager.find(entityClass, id);
    }

    protected void remove(T entity) {
        entityManager.remove(entityManager.merge(entity));
    }

    protected void removeById(U id) {
        T entity = findById(id);
        remove(entity);
    }
}
