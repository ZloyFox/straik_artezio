package ru.artezio.dao.impl;

import org.springframework.stereotype.Repository;
import ru.artezio.dao.HousingDao;
import ru.artezio.model.Housing;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class HousingDaoImpl extends GenericDaoImpl<Housing, Long> implements HousingDao {

    @Override
    public List<Housing> list() {
        return findObjectsByNamedQuery("findAllHousing");
    }

    @Override
    public Housing create(Housing housing) {
        return save(housing);
    }

    @Override
    public Housing update(Housing housing) {
        return super.update(housing);
    }

    @Override
    public Housing findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void remove(Housing housing) {
        super.remove(housing);
    }

    @Override
    public void remove(Long id) {
        super.removeById(id);
    }
}
