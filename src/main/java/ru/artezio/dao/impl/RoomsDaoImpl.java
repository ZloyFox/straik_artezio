package ru.artezio.dao.impl;

import org.springframework.stereotype.Repository;
import ru.artezio.dao.RoomsDao;
import ru.artezio.model.Rooms;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class RoomsDaoImpl extends GenericDaoImpl<Rooms, Long> implements RoomsDao {

    @Override
    public List<Rooms> list() {
        return findObjectsByNamedQuery("findAllRooms");
    }

    @Override
    public Rooms create(Rooms rooms) {
        return save(rooms);
    }

    @Override
    public Rooms update(Rooms rooms) {
        return super.update(rooms);
    }

    @Override
    public Rooms findById(Long id) {
        return super.findById(id);
    }

    @Override
    public void remove(Rooms rooms) {
        super.remove(rooms);
    }

    @Override
    public void remove(Long id) {
        super.removeById(id);
    }
}
