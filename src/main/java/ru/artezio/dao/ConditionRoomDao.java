package ru.artezio.dao;

import ru.artezio.model.ConditionRoom;

import java.util.List;

public interface ConditionRoomDao {

    List<ConditionRoom> list();

    public ConditionRoom create(ConditionRoom conditionRoom);

    public ConditionRoom update(ConditionRoom conditionRoom);

    public ConditionRoom findById(Long id);

    public void remove(ConditionRoom conditionRoom);

    public void remove(Long id);
}
