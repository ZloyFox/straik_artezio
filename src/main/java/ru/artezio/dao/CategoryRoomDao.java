package ru.artezio.dao;

import ru.artezio.model.CategoryRoom;

import java.util.List;

public interface CategoryRoomDao {

    List<CategoryRoom> list();

    public CategoryRoom create(CategoryRoom categoryRoom);

    public CategoryRoom update(CategoryRoom categoryRoom);

    public CategoryRoom findById(Long id);

    public void remove(CategoryRoom categoryRoom);

    public void remove(Long id);
}
