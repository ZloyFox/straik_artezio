package dao;

import context.TestConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.artezio.dao.UserDao;
import ru.artezio.model.User;
import ru.artezio.model.UserRole;
import ru.artezio.model.enumerated.UserStatus;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@Transactional
public class UserDaoTest extends TestConfiguration{

    @PersistenceContext private EntityManager entityManager;
    @Autowired private UserDao userDao;
    private static final String FIND_BY_LOGIN = "select * from users where login = ?";
    private static final String FIND_BY_ID = "select * from users where id = ?";
    private static final String LIST_USERS = "select * from users";

    @Test
    public void testSaveRecord() throws Exception {
        User user = new User("testLogin", "testPassword", "uandex@jh", UserStatus.ACTIVE);
        user.setRoles(createSetUserRoles());

        user = userDao.create(user);
        assertNotNull(user.getId());
        Query query = entityManager.createNativeQuery(FIND_BY_ID, User.class);
        query.setParameter(1, user.getId());
        User user1 = (User) query.getSingleResult();

        assertTrue(user1.equals(user));
    }

    @Test
    public void testDeleteRecord() throws Exception {
        Query query = entityManager.createNativeQuery(FIND_BY_LOGIN, User.class);
        query.setParameter(1, "testLoginDelete");
        User user = (User) query.getSingleResult();
        user.setRoles(createSetUserRoles());
        userDao.remove(user);

        assertTrue(query.getResultList().isEmpty());
    }

    @Test
    public void testFindByIdRecord() throws Exception {
        Query query = entityManager.createNativeQuery(FIND_BY_LOGIN, User.class);
        query.setParameter(1, "testLoginFindId");
        User user = (User) query.getSingleResult();
        user.setRoles(createSetUserRoles());
        assertTrue(userDao.findById(user.getId()).equals(user));
    }

    @Test
    public void testUpdateRecord() throws Exception {
        User user = new User("testLoginUpdate", "testPassword", "uandex@jh", UserStatus.ACTIVE);
        user.setRoles(createSetUserRoles());
        User user2 = userDao.create(user);
        user2.setPassword("updateTestPassword");
        userDao.create(user2);

        Query query = entityManager.createNativeQuery(FIND_BY_LOGIN, User.class);
        query.setParameter(1, "testLoginUpdate");
        user = (User) query.getSingleResult();

        assertTrue(user.equals(user2));
    }

    @Test
    public void testGetAll() throws Exception {
        List<User> users = userDao.list();
        Query query = entityManager.createNativeQuery(LIST_USERS, User.class);
        List<User> list = query.getResultList();

        assertNotNull(users);
        assertTrue(list.equals(users));
    }

    @Test
    public void testFindByLogin() throws Exception {
        Query query = entityManager.createNativeQuery(FIND_BY_LOGIN, User.class);
        String testLogin = "testLoginFindLogin";
        query.setParameter(1, testLogin);
        User user = (User) query.getSingleResult();

        assertTrue(userDao.findByLogin(testLogin).equals(user));
    }

    private Set<UserRole> createSetUserRoles() {
        Set<UserRole> set = new HashSet<UserRole>();
        UserRole userRole = new UserRole("Admin");
        userRole.setId(1L);
        set.add(userRole);
        userRole = new UserRole("User");
        userRole.setId(2L);
        set.add(userRole);
        return set;
    }

}
